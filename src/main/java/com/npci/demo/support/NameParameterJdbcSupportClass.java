package com.npci.demo.support;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;

public class NameParameterJdbcSupportClass extends NamedParameterJdbcDaoSupport {
	@Autowired
	public void setDataSources(DataSource dataSource) {
		setDataSource(dataSource);
	}
}
